
let outId = document.getElementById('text');

let eventObserverFocus = () => {
    outId.style.cssText = `
    border-color: green;
    box-shadow: 5px 5px 5px  green;
    `
};

let eventObserverBlur = () => {
    outId.style.cssText = ``
    let validError = document.getElementById('error');
    if (validError) {
        validError.remove()

    };
};

let createSpane = (value) => {

    let validError = document.getElementById('error');
    if (validError) {
        validError.remove();
    }
    let crateSpanWithValue = document.createElement('span');
    crateSpanWithValue.className = 'presentValue';
    crateSpanWithValue.textContent = `Текущая цена: ${value}`;
    document.getElementById('text').before(crateSpanWithValue);
    nestedSpan();
};

let nestedSpan = () => {
    let closes = document.createElement('span');
    closes.classList.add('closesIcones');
    let classNameParent = document.querySelectorAll('.presentValue');
    classNameParent.forEach(elem => elem.append(closes));
};

let validateInputValue = () => {
    let result = document.getElementById('error');
    if (result) {
        result.id = 'error';
        result.textContent = 'Please enter correct price';
        outId.style.cssText = `
        border-color: red;
        box-shadow: 5px 5px 5px red;
              `;
    } else {
        let createP = document.createElement('p');
        createP.id = 'error';
        createP.textContent = 'Please enter correct price';
        createP.style.color = 'red';
        document.getElementById('text').after(createP);
        outId.style.cssText = `
    border-color: red;
    box-shadow: 5px 5px 5px red;
          `;
    }
};


let showOut = () => {
    let inputValue = document.getElementById('text').value;
    if (!inputValue) {
        eventObserverBlur();
    } else if (inputValue <= 0) {
        validateInputValue();
    } else {
        createSpane(inputValue);
    }

};

let closesElement = () => {
    let removeButtons = document.querySelectorAll('.closesIcones');
    removeButtons.forEach(elem => {
        elem.addEventListener('click', (event) => {
            event.target.closest('.presentValue').remove();
        })
    })
};

document.addEventListener('click', closesElement);
outId.addEventListener('focus', eventObserverFocus);
outId.addEventListener('blur', showOut);
