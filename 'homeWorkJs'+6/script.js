// forEach() это цикл перебора в массивах 




let filterBy = (arr, typeOf) => arr.filter(elem =>{
	if (typeOf === 'null') return elem !== null;
	return typeof elem !== typeOf || elem === null;

});
let arr1 = ['hello', 'world', 23, true, undefined, 1n, Symbol(), {}, function () { }, null];

let userTypeOf = ['number', 'string', 'bigint', 'boolean', 'undefined', 'symbol', 'object', 'null'];

userTypeOf.forEach(elem => console.log(filterBy(arr1, elem)));




