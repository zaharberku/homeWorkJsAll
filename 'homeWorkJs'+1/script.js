
// 1.Первым чем отличается let от var это - var мы можем изменить, как в ограниченом блоке, так и за его пределами, для него просто 
// не существует блочной области видимости, let же 
// обьявляет переменную только в области видимости ограниченым текущим блоком кода.Второе, не можно обьявить let с такой же переменной 
// в одной области видимости. Третье - let ввидна только после обьявления переменной.
// Сonst мы не можем изменять в отличии от let и var. 
// 2. Так как var есть устарелым способом обьявления переменной.

'use strict'

let userName = prompt('Введите ваше имя');
while (!userName) {
    userName = prompt('Введите ваше имя ещё раз!');
}
let userAge = prompt('Введите ваш возраст')
while (!userAge || Number.isNaN(+userAge)) {
    userAge = prompt('Введите ваше возраст ещё раз!');
}
if (userAge < 18) {
    alert('You are not allowed to visit this website.');
} else if (userAge >= 18 && userAge <= 22) {
    let userAnswer = confirm('Are you sure you want to continue?');
    if (userAnswer) {
        alert(`Welcome, ${userName}`);
    } else {
        alert('You are not allowed to visit this website.');
    }
} else {
    alert(`Welcome, ${userName}`);
}

