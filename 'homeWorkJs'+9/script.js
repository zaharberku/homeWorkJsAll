

let slideSwitch = (event) => {
    let classDataSet = document.querySelector(`.tabs-content li[data-content="${event.target.dataset.item}"]`)
    event.target.closest('.tabs').querySelector('.active').classList.remove('active')
    classDataSet.closest('.tabs-content').querySelector('.active').classList.remove('active')
    classDataSet.classList.add('active')
    event.target.classList.add('active');
}



document.addEventListener('click', slideSwitch)





