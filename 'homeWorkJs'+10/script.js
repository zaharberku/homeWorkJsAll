let showIconPassword = document.querySelectorAll('.fa-eye');
let textError = document.querySelector('.not-error');
let mainForm = document.forms[0]
let inputEnterPassword = mainForm.inputFormFirst
let inputConfirmPassword = mainForm.inputFormSecond;
let span = document.querySelector('.error-password')

let numberOfLetters = () => {

    if (inputEnterPassword.value.length <= 6) {
        span.classList.add('block');

    } else {
        span.classList.remove('block');

    }
};

inputEnterPassword.addEventListener('input', numberOfLetters);

mainForm.onsubmit = () => false;

let validValueInput = (event) => {
    if (inputEnterPassword.value.length <= 6) {
        event.preventDefault();
    } else if (inputEnterPassword.value !== '' && inputConfirmPassword.value !== '' && inputEnterPassword.value === inputConfirmPassword.value) {
        textError.classList.add('not-error');
        alert('You are welcome');
        mainForm.reset();
    } else {
        textError.classList.remove('not-error');
        event.preventDefault();
    };
}
mainForm.addEventListener('submit', validValueInput);


let showingPassword = (event) => {
    if (event.target.closest('.input-wrapper').querySelector('input').getAttribute('type') === 'text') {
        event.target.closest('.input-wrapper').querySelector('input').setAttribute('type', 'password');
    } else {
        event.target.closest('.input-wrapper').querySelector('input').setAttribute('type', 'text');
    };
    event.target.classList.toggle('fa-eye-slash');
};

showIconPassword.forEach(element => {
    element.addEventListener('click', showingPassword);
});







