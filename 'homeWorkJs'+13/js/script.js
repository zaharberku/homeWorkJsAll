let showBtnChangeTopic = document.querySelector('.btn-change-topic');
let showNewStyle = document.querySelector('.link-new-style');



let validChangeTopic = () => {
   if (localStorage.getItem('background')) {
    showNewStyle.setAttribute('href', 'css/nightStyle.css')
   } 
};



let changeTopic = ()=>{
    if (showNewStyle.getAttribute('href') === 'css/style.css') {
     showNewStyle.setAttribute('href', 'css/nightStyle.css')
     localStorage.setItem('background','true')
    }else{
     showNewStyle.setAttribute('href', 'css/style.css')
     localStorage.removeItem('background')
    }
 };

showBtnChangeTopic.addEventListener('click', changeTopic);
validChangeTopic();