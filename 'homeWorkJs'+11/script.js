

document.addEventListener('keyup', (event) => {
    event.target.querySelectorAll('.btn').forEach(element => {
        let dataValue = `Key${element.dataset.btn}`
        if (dataValue === event.code || element.dataset.btn === event.code) {
            if (document.querySelector('.btn.background-btn')) {
                document.querySelector('.btn.background-btn').classList.remove('background-btn')
            }
            element.classList.add('background-btn')
        }

    })
})